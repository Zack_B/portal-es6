import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import NewAccountContainer from './NewAccountContainer';
import NewExternalAccountContainer from './NewExternalAccountContainer';
import { Router, Route, browserHistory } from 'react-router';
import './index.css';
import '../node_modules/zurb-foundation-npm/css/foundation.css';

ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <Route path="new-account" component={NewAccountContainer}/>
      <Route path="new-external-account" component={NewExternalAccountContainer}/>

    </Route>
  </Router>,
  document.getElementById('root')
);
