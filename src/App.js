import React, { Component } from 'react';
import Panel from './Panel';

import { Link } from 'react-router';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataLoaded : true
    };
  }

  componentDidMount(){
    // Fetch the data
  }

  render() {
    return (
      <div style={{ width: '100%' }}>
        <div className="row">
          <h1>Welcome to Live Oak Bank <small>(there should be a header here)</small></h1>
        </div>
        <div className="row">
          <div className="columns medium-3">
            
          </div>
          <div className="columns medium-6">
            <Panel type="twoButtons">
              <button style={{ margin: '1%', background: 'white'}} ><Link to="/new-account">Open a new account</Link></button>
              <button style={{ margin: '1%', background: 'white'}}><Link to="/new-external-account">Link an external account</Link></button>
            </Panel>
            {this.props.children}
          </div>
          <div className="columns medium-3">

          </div>
        </div>
      </div>
    );
  }
}

export default App;
