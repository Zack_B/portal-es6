import React, { Component } from 'react';
import Panel from './Panel';
import Row from './Row';
import AccountButton from './AccountButton';

class NewAccountContainer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedProductType: null,
      selectedProduct: null,
      selectedProductCategory: null,
      typeSelected : null
    };
  }

  updateSelectedAccount(type){
    console.log('updated');
    this.setState({typeSelected: type });
  }

    render() {
        return (
          <section id="NewAccountContainer">
            <Panel>
              <Row>
                <div className="columns large-12">
                  <h1 className="text-center">
                    New Account Type
                  </h1>
                </div>
              </Row>
              <Row>
                <AccountButton
                  typeLabel="Business Account"
                  type="business"
                  clickAction={this.updateSelectedAccount.bind(this)}
                  typeSelected={this.state.typeSelected} />
                <AccountButton
                  typeLabel="Personal Account"
                  type="personal"
                  clickAction={this.updateSelectedAccount.bind(this)}
                  typeSelected={this.state.typeSelected}  />
              </Row>
              <Row>
                <h2 style={{textAlign : 'center'}}>{this.state.typeSelected}</h2>
              </Row>
            </Panel>
          </section>
        );
    }
}

export default NewAccountContainer;
