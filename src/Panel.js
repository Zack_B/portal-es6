import React, { Component } from 'react';

class Panel extends Component {
    constructor(props){
      super(props);
      this.state = {
        type : this.props.type
      };

    }

    getLayout(){
      let style = {};
      if (this.state.type === 'twoButtons') {
        style = {
          textAlign : 'center',
          background : '#555555'
        };
      }
      return style;
    }

    render() {
        return (
            <div className="panel" style={ this.getLayout() }>
                {this.props.children}
            </div>
        );
    }
}

export default Panel;
