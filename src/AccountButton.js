import React, { Component } from 'react';
import classNames from 'classnames/bind';
class AccountButton extends Component {

    buttonClicked(){
      this.props.clickAction( this.props.type );
    }

    render() {
        return (
            <div className={classNames({'acctTypeButton': true, 'selected' : this.props.typeSelected === this.props.type })} onClick={this.buttonClicked.bind(this)}>
                {this.props.typeLabel}
            </div>
        );
    }
}

export default AccountButton;
