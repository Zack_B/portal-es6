import React, { Component } from 'react';

class AccountList extends Component {
    render() {
        <div id="AccountList">
            <div>
                My Accounts
            </div>
            <div>
                <ul style={{ display: 'flex', alignContent: 'space-around' }}>
                    <li>All</li>
                    <li>Business</li>
                    <li>Personal</li>
                </ul>
            </div>
            
        </div>
    }
}

export default AccountList;